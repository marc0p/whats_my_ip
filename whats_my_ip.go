package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"text/template"
	"time"
)

type ResponseData struct {
	Status string   `json:"status"`
	Data   Datatype `json:"data"`
}

type Datatype struct {
	Ip string `json:"ip"`
}

type TemplateData struct {
	Timestamp string
	IpAddress string
}

func main() {
	client := new(http.Client)

	baseurl := "https://stat.ripe.net/data/whats-my-ip/data.json"

	request, err := http.NewRequest("GET", baseurl, nil)
	if err != nil {
		panic(err)
	}

	// Add the parameters to the query
	query := request.URL.Query()

	// encode the query to be url safe
	request.URL.RawQuery = query.Encode()

	response, err := client.Do(request)
	if err != nil {
		fmt.Printf("Failed to get %s: %s", baseurl, err.Error())
		panic(err)
	}

	// Read all of the response body into []byte
	body, err := ioutil.ReadAll(response.Body)
	defer response.Body.Close()
	if err != nil {
		panic(err)
	}

	// declare a variable for storing the unmarshalled the json data
	var responseJson ResponseData

	// unmarshal the []byte of the body into the variable (notice the reference)
	err = json.Unmarshal(body, &responseJson)
	if err != nil {
		panic(err)
	}

	timestamp := strconv.FormatInt(time.Now().UTC().Unix(), 10)

	t := template.Must(template.New("zone.tmpl").ParseFiles("zone.tmpl"))

	f, err := os.Create("/tmp/zone")
	if err != nil {
		fmt.Println(err)
		return
	}

	p := TemplateData{Timestamp: string(timestamp), IpAddress: string(responseJson.Data.Ip)}

	err2 := t.Execute(f, p)
	if err2 != nil {
		fmt.Println(err2)
	}

}
